jQuery(document).ready(function(){
		
		setBlockDisplay()
		
		$('#block-head').on('click', function(){
			//console.debug ('was clicked');
			if ($('#all-content').css('display') == 'none'){
				 //console.debug('Show block');
				 $('#all-content').animate({height: 'show'}, 200);
				 setCookie('display', 'block');
			}else{
				//console.debug('Hide block');
				$('#all-content').animate({height: 'hide'}, 200);
				setCookie('display', 'none');
			}	
		});
	});


function setCookie(name, value) {
  //value = encodeURIComponent(value);
  var updatedCookie = name + "=" + value;
  document.cookie = updatedCookie;
}

function setBlockDisplay(){
	var blockStatus = 'none';
		for (i in document.cookie.split('; '))
		{
			if (document.cookie.split('; ')[i].split('=')[0] == "display")
				blockStatus = document.cookie.split('; ')[i].split('=')[1];
				//console.debug(blockStatus);
		}

		if (blockStatus == "block"){
			//console.debug("Show block");
			document.getElementById("all-content").style.display = 'block';
		}
		else{
			//console.debug("Hide block");
			document.getElementById("all-content").style.display = 'none';
		}	
}

